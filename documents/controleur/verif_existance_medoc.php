<?php
header("Access-Control-Allow-Origin: *");
	// $_POST['ville']=28;
	// $_POST['medicament1']='paracetamol';
	// $_POST['medicament2']='efferalgan';
	// $_POST['latitude']=0.41850479999999995;
	// $_POST['longitude']=9.4295191;
	if(isset($_POST)){
		include_once('../model/bigModelForMe.php');
		$tab_pharmacies = array();
		$ville = $_POST['ville'];
		$id_ville = $manager->selectionUnique2('ville',array('id_ville'),"nom_ville='$ville'");
		$id_ville = $id_ville[0]->id_ville;
		
		$tab_pharmacies = $manager->selectionUnique2('pharmacies',array('*'),"ville_id=$id_ville");
		
		$id_pays = $manager->selectionUnique2('ville',array('p_num'),"nom_ville='$ville'");
		$id_pays = $id_pays[0]->p_num;
		//*****classe en fonction des distances********
			$lesdistances = array();
			$lesdistancescroissantes = array();
			$tab_pharmacies2 = array();
			$latuser = ($_POST['latitude']*M_PI)/180;
			$longuser = ($_POST['longitude']*M_PI)/180;
			//********tableau contenant les phamarcies et leurs distances*****
				foreach($tab_pharmacies as $key=>$val):
					$latpharma;
					$longpharma;
					$nompharma;
					$distance = 0;
					foreach($val as $k=>$v){
						if($k =='latitude'){
							$v2 = floatval($v);
							$latpharma = ($v2*M_PI)/180;
						}else if($k =='longitude'){
							$v3 = floatval($v);
							$longpharma = ($v3*M_PI)/180;
						}else if($k == 'nom'){
							$nom = $v;
						}
					}
					
					$distance = sqrt(pow(($longuser-$longpharma),2)+pow(($latuser-$latpharma),2))*6371; //rayon de la terre r=6371
					$lesdistances[$nom] = $distance;
				endforeach;
			//***********************************
			$verifie = 0;
			//****classe les pharmacies par ordre croissant******
			for($i=0;$i <count($lesdistances)-1;$i++){
				if(count($lesdistancescroissantes) != count($lesdistances)){
					foreach($lesdistances as $key=>$val):
						if($val != 0){
							$dimoi = 0;
							foreach($lesdistances as $k=>$v){
								if($v !=0 AND $val <= $v){
									$dimoi++;
								}
							}
							if($dimoi == count($lesdistances)-$verifie){
								$lesdistancescroissantes[$key] = $val;
								$val = 0;
								$verifie++;
							}
						}
					endforeach;
				}
			}
			//**************************
			//*********recree le tableau tab_pharmacies en ordre de distance croissante***
					$comptage =0;
				foreach($lesdistancescroissantes as $key=>$val):
					foreach($tab_pharmacies as $k=>$v){
						foreach($v as $kk=>$vv){
							if($key == $vv){
								$tab_pharmacies2[$comptage]= $tab_pharmacies[$k];
							}
						}
					}
					$comptage++;
				endforeach;
			//**********************
		//****************************************
		$tab_coordonnees = $manager->selection('coordonnees',array('*'));
		$tab_medoc = array();//tableau contenant les medoc cherchés
		$big_table = array();
		for($i = 1; $i <= count($_POST)-3;$i++){
			$tab_medoc[] = $_POST['medicament'.$i];
		}
		 // tableau qui contiendra la liste des medoc trouves
		$renvoi = array();
		for($i = 0; $i < count($tab_pharmacies); $i++){ //boucle chaque pharmacie
			$non_trouve = array();
			$nom = array();
			$prix = array();
			$quantite = array();
			$coordonnees = array();
			// $coordonnees[] = $tab_coordonnees[$i]->latitude;
			// $coordonnees[] = $tab_coordonnees[$i]->longitude;
			$db3 = new PDO('mysql:host=mysql.hostinger.fr;dbname=u228449069_bd2','u228449069_franc','bd2@franc1993');
			// $findumot = '';
			// if($id_pays == 2){
				// $findumot ='gb';
			// }else if($id_pays == 1){
				// $findumot = 'cmr';
			// }
			
			if($tab_pharmacies[$i]){ //si je sui sur une pharmacie
				for($j =0 ;$j < count($tab_medoc);$j++){ // verifie chaque medoc dans la pharmacie
					$valeur = $db3->query("select * from tab_".$tab_pharmacies[$i]->nom." where produit like '%$tab_medoc[$j]%'");
					$valeur = $valeur->fetch();
					if($valeur['produit'] != ''){
						$nom[] = $valeur['produit'];
						$prix[] = $valeur['prix'];
						$quantite[] = $valeur['quantite'];
					}else{
						$non_trouve[] = $tab_medoc[$j];
					}
				}
					if(count($nom)!= 0){
						$renvoi[] = array($tab_pharmacies[$i]->nom => $nom,'prix'=> $prix,'quantite'=> $quantite,'coordonnees'=>$coordonnees,'manquant' => $non_trouve);
					}
			}
		}
		echo json_encode($renvoi);
	}
?>