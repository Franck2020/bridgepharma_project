<?php
header("Access-Control-Allow-Origin: *");
	if(isset($_FILES) AND isset($_POST)){
		include_once('../model/bigModelForMe.php');
		$nbre = count($_FILES);
		$datess = date("Y-m-d H:i:s");
		$num_users = $_POST['num_users'];
		$nom_pharmacie = $_POST['nom_pharmacie'];
		$pays = $_POST['pays'];
		$verifie = 0;
		$noms_photos = array();
		foreach($_FILES as $n){
				//Testons si les fichiers ne sont pas trop gros
				if ($n['size'] <= 1000000 AND $n['error'] == 0){
					//Testons si l'extension est autorisée
					$infosfichier =pathinfo($n['name']);
					$noms_photos[] = $n['name'];
					$extension_upload = $infosfichier['extension'];
					$extensions_autorisees = array('jpg', 'JPG','jpeg','JPEG','GIF', 'gif','png','PNG');
				if (in_array($extension_upload,$extensions_autorisees)){
					//On peut valider le fichier et le stocker définitivement
					move_uploaded_file($n['tmp_name'], '../photosordonnances/'.basename($n['name']));
					$verifie += 1;
				}
			}
		}
		if($verifie == $nbre){
			$n = $noms_photos[0];
			if($pays == 'Cameroun'){
				$nom_pharmacie = $nom_pharmacie.'_cmr';
			}else if($pays == 'Gabon'){
				$nom_pharmacie = $nom_pharmacie.'_gb';
			}
			$tab = $manager->selectionUnique2('pharmacies',array('num_pharmacies'),"nom ='$nom_pharmacie'");
			$tab = $tab[0]->num_pharmacies;
			$table1 = array(
					'users_num'=>$num_users,
					'datess'=>$datess,
					'pharmacies_num'=>$tab
				);
				$manager->insertion('commandes',$table1);
				$id = $manager->dernierIdInserer();
				
			$table2 = array(
					'produits'=>'',
					'quantites'=>'',
					'photos'=>$n,
					'cmd_num'=>$id
				);
			 $manager->insertion('produits',$table2);
			echo json_encode('bien effectues');
		}
	}else{
		echo  json_encode('ok');
	}
?>