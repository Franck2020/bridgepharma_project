-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 27 Juillet 2016 à 18:44
-- Version du serveur :  5.6.17-log
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `pharmacie_de_garde`
--

-- --------------------------------------------------------

--
-- Structure de la table `tab_pharmacie_de_garde`
--

CREATE TABLE IF NOT EXISTS `tab_pharmacie_de_garde` (
  `tab_for` int(11) NOT NULL AUTO_INCREMENT,
  `produit` varchar(40) DEFAULT NULL,
  `prix` int(11) DEFAULT NULL,
  `quantite` int(11) NOT NULL,
  PRIMARY KEY (`tab_for`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `tab_pharmacie_de_garde`
--

INSERT INTO `tab_pharmacie_de_garde` (`tab_for`, `produit`, `prix`, `quantite`) VALUES
(1, 'paracetamol', 500, 4),
(2, 'efferalgan', 500, 6),
(3, 'aspirine', 1000, 3);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
