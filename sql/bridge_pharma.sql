-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 27 Juillet 2016 à 18:43
-- Version du serveur :  5.6.17-log
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `bridge_pharma`
--

-- --------------------------------------------------------

--
-- Structure de la table `attente_code`
--

CREATE TABLE IF NOT EXISTS `attente_code` (
  `num_att_cd` int(11) NOT NULL AUTO_INCREMENT,
  `medicaments` text,
  `quantite` text,
  `dates` datetime DEFAULT NULL,
  `users_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`num_att_cd`),
  KEY `users_num` (`users_num`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `attente_code`
--

INSERT INTO `attente_code` (`num_att_cd`, `medicaments`, `quantite`, `dates`, `users_num`) VALUES
(1, 'paracetamol,aspirine', '1,1', '2016-07-22 22:52:50', 14),
(2, 'gh,hgj,kjhgk', '1,1,1', '2016-07-27 11:27:03', 14);

-- --------------------------------------------------------

--
-- Structure de la table `commandes`
--

CREATE TABLE IF NOT EXISTS `commandes` (
  `num_cmd` int(11) NOT NULL AUTO_INCREMENT,
  `produit` varchar(30) DEFAULT NULL,
  `quantite` int(6) DEFAULT NULL,
  `reservation_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`num_cmd`),
  KEY `reservation_num` (`reservation_num`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Contenu de la table `commandes`
--

INSERT INTO `commandes` (`num_cmd`, `produit`, `quantite`, `reservation_num`) VALUES
(33, 'paracetamol', 1, 21),
(34, 'aspirine', 1, 21),
(35, 'migralgine', 1, 22),
(36, 'doliprane', 1, 23),
(37, 'paracetamol', 1, 24);

-- --------------------------------------------------------

--
-- Structure de la table `coordonnees`
--

CREATE TABLE IF NOT EXISTS `coordonnees` (
  `ph_coo` int(11) NOT NULL AUTO_INCREMENT,
  `latitude` varchar(20) DEFAULT NULL,
  `longitude` varchar(20) DEFAULT NULL,
  `pharmacie_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`ph_coo`),
  KEY `pharmacie_num` (`pharmacie_num`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `coordonnees`
--

INSERT INTO `coordonnees` (`ph_coo`, `latitude`, `longitude`, `pharmacie_num`) VALUES
(1, '0.22334433', '0.33848485', 1),
(2, '-0.33322', '0.94485454', 2),
(3, '0.956754', '4.5685', 3);

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

CREATE TABLE IF NOT EXISTS `groupe` (
  `num_groupe` int(11) NOT NULL AUTO_INCREMENT,
  `lib_groupe` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`num_groupe`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `groupe`
--

INSERT INTO `groupe` (`num_groupe`, `lib_groupe`) VALUES
(1, 'client'),
(2, 'admin');

-- --------------------------------------------------------

--
-- Structure de la table `pharmacies`
--

CREATE TABLE IF NOT EXISTS `pharmacies` (
  `num_pharmacies` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) DEFAULT NULL,
  `photo1` varchar(200) DEFAULT NULL,
  `photo2` varchar(200) DEFAULT NULL,
  `photo3` varchar(200) DEFAULT NULL,
  `photo4` varchar(200) DEFAULT NULL,
  `horaire` text,
  `contact` varchar(15) DEFAULT NULL,
  `position` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`num_pharmacies`),
  FULLTEXT KEY `horaire` (`horaire`),
  FULLTEXT KEY `horaire_2` (`horaire`),
  FULLTEXT KEY `horaire_3` (`horaire`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `pharmacies`
--

INSERT INTO `pharmacies` (`num_pharmacies`, `nom`, `photo1`, `photo2`, `photo3`, `photo4`, `horaire`, `contact`, `position`) VALUES
(1, 'pharmacie_des_forestiers', 'index.jpg', 'index3.jpg', 'index4.jpg', 'index5.jpg', 'lundi au vendredi 8h15 a 19h45.samedi 8h30 a 1930/dimanche 9h30 a 13h30', '01722352', 'Au quartier Vallee Sainte-Marie a la Galerie marchande de M''bolo\r\n'),
(2, 'pharmacie_de_garde', NULL, NULL, NULL, NULL, '7h 30', '02565445', 'hhhhhhhhhhhhhhhhhhhhhhhhh'),
(3, 'pharmacie_commissariat', NULL, NULL, NULL, NULL, '8h', '07786868', 'En face du commissariat central');

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

CREATE TABLE IF NOT EXISTS `reservation` (
  `num_reservation` int(11) NOT NULL AUTO_INCREMENT,
  `date_debut_reservation` datetime DEFAULT NULL,
  `date_fin_reservation` datetime DEFAULT NULL,
  `users_num` int(11) DEFAULT NULL,
  `pharmacies_num` int(11) DEFAULT NULL,
  `heure` time NOT NULL,
  `identifiant` varchar(20) NOT NULL,
  PRIMARY KEY (`num_reservation`),
  KEY `users_num` (`users_num`),
  KEY `pharmacies_num` (`pharmacies_num`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Contenu de la table `reservation`
--

INSERT INTO `reservation` (`num_reservation`, `date_debut_reservation`, `date_fin_reservation`, `users_num`, `pharmacies_num`, `heure`, `identifiant`) VALUES
(21, '2016-07-27 11:25:00', '2016-07-27 13:25:00', 14, 2, '13:25:00', '5712310'),
(22, '2016-07-27 11:25:00', '2016-07-27 13:25:00', 14, 3, '13:25:00', '2713025'),
(23, '2016-07-27 11:34:01', '2016-07-27 13:34:01', 14, 1, '13:34:01', '7103523'),
(24, '2016-07-27 11:34:01', '2016-07-27 13:34:01', 14, 2, '13:34:01', '3704215');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE IF NOT EXISTS `utilisateur` (
  `num_users` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) DEFAULT NULL,
  `prenom` varchar(20) DEFAULT NULL,
  `date_de_naissance` date DEFAULT NULL,
  `sexe` varchar(10) DEFAULT NULL,
  `telephone` varchar(15) DEFAULT NULL,
  `passwordss` varchar(200) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `photo` varchar(200) NOT NULL,
  `ville_gabon_id` int(11) DEFAULT NULL,
  `groupe_num` int(11) DEFAULT NULL,
  `date_inscription` datetime DEFAULT NULL,
  PRIMARY KEY (`num_users`),
  KEY `ville_gabon_id` (`ville_gabon_id`),
  KEY `groupe_num` (`groupe_num`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`num_users`, `nom`, `prenom`, `date_de_naissance`, `sexe`, `telephone`, `passwordss`, `email`, `photo`, `ville_gabon_id`, `groupe_num`, `date_inscription`) VALUES
(14, 'bg', 'nbgbdg', '2016-06-10', 'm', '123', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'dd@ff', 'IMAG0145.jpg', 4, 1, '2016-06-15 18:34:15');

-- --------------------------------------------------------

--
-- Structure de la table `ville_gabon`
--

CREATE TABLE IF NOT EXISTS `ville_gabon` (
  `id_ville_gabon` int(4) NOT NULL AUTO_INCREMENT,
  `nom_ville` varchar(30) NOT NULL,
  PRIMARY KEY (`id_ville_gabon`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=59 ;

--
-- Contenu de la table `ville_gabon`
--

INSERT INTO `ville_gabon` (`id_ville_gabon`, `nom_ville`) VALUES
(1, 'Akanda'),
(2, 'Akok'),
(3, 'Bakoumba'),
(4, 'BÃ©linga'),
(5, 'Bifoun'),
(6, 'Bikele'),
(7, 'Bitam'),
(8, 'Bongoville'),
(9, 'BoouÃ©'),
(10, 'Boumango'),
(11, 'Cap EstÃ©rias'),
(12, 'Cocobeach'),
(13, 'Dienga'),
(14, 'Donguila'),
(15, 'Engo Effack'),
(16, 'Enyonga'),
(17, 'Ã‰tÃ©kÃ©'),
(18, 'Fougamou'),
(19, 'Franceville'),
(20, 'Gamba'),
(21, 'Kango'),
(22, 'Kougouleu'),
(23, 'Koulamoutou'),
(24, 'LambarÃ©nÃ©'),
(25, 'Lastoursville'),
(26, 'LÃ©bamba'),
(27, 'LÃ©koni'),
(28, 'Libreville'),
(29, 'Makokou'),
(30, 'Mandji'),
(31, 'Mayemba'),
(32, 'Mayibout'),
(33, 'Mayumba'),
(34, 'Mbigou'),
(35, 'MÃ©douneu'),
(36, 'MÃ©kambo'),
(37, 'Mimongo'),
(38, 'Minvoul'),
(39, 'Mitzic'),
(40, 'Moabi'),
(41, 'Moanda'),
(42, 'Mouila'),
(43, 'Mounana'),
(44, 'NdendÃ©'),
(45, 'NdjolÃ©'),
(46, 'Ndzomoe'),
(47, 'Nkan'),
(48, 'Ntoum'),
(49, 'Okondja'),
(50, 'OmbouÃ©'),
(51, 'Onga'),
(52, 'Owendo'),
(53, 'Oyem'),
(54, 'Petit Loango'),
(55, 'Port-Gentil'),
(56, 'Tchibanga'),
(57, 'Tsogni'),
(58, 'Yenzi');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `attente_code`
--
ALTER TABLE `attente_code`
  ADD CONSTRAINT `attente_code_ibfk_1` FOREIGN KEY (`users_num`) REFERENCES `utilisateur` (`num_users`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `commandes`
--
ALTER TABLE `commandes`
  ADD CONSTRAINT `commandes_ibfk_1` FOREIGN KEY (`reservation_num`) REFERENCES `reservation` (`num_reservation`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `coordonnees`
--
ALTER TABLE `coordonnees`
  ADD CONSTRAINT `coordonnees_ibfk_1` FOREIGN KEY (`pharmacie_num`) REFERENCES `pharmacies` (`num_pharmacies`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `reservation_ibfk_1` FOREIGN KEY (`users_num`) REFERENCES `utilisateur` (`num_users`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reservation_ibfk_2` FOREIGN KEY (`pharmacies_num`) REFERENCES `pharmacies` (`num_pharmacies`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `utilisateur_ibfk_1` FOREIGN KEY (`ville_gabon_id`) REFERENCES `ville_gabon` (`id_ville_gabon`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `utilisateur_ibfk_2` FOREIGN KEY (`groupe_num`) REFERENCES `groupe` (`num_groupe`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
