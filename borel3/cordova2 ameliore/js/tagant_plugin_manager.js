 (function($){
	//**********code servant a stopper,avancer,faire reculer un caroussel
		var carroussel = {
			play: function(){
					this.timer = setInterval(function(){
					var nbre = $('#carroussel .slide').get().length;
					var elem = $('#carroussel .slide').eq(nbre-1);
					var elem2 = $('#carroussel .slide.active');
					if(elem.attr('name') == elem2.attr('name')){
						elem2.hide();
						elem2.removeClass('active');
						$('#carroussel .slide').eq(0).addClass('active');
						$('#carroussel .slide').eq(0).show();
					}else{
						var suivant = elem2.next();
						elem2.hide();
						elem2.removeClass('active');
						suivant.addClass('active');
						suivant.show();
					}
				},3000);
			},
			stop: function(){
				window.clearInterval(this.timer);
			},
			next: function(){
				this.stop();
				var nbre = $('#carroussel .slide').get().length;
				var elem = $('#carroussel .slide').eq(nbre-1);
				var elem2 = $('#carroussel .slide.active');
				if(elem.attr('name') == elem2.attr('name')){
					
				}else{
					var suivant = elem2.next();
					elem2.hide();
					elem2.removeClass('active');
					suivant.addClass('active');
					suivant.show();
				}
			},
			prev: function(){
				this.stop();
				var nbre = $('#carroussel .slide').get().length;
				var elem = $('#carroussel .slide').eq(0);
				var elem2 = $('#carroussel .slide.active');
				if(elem.attr('name') == elem2.attr('name')){
				}else{
					var precedent = elem2.prev();
					elem2.hide();
					elem2.removeClass('active');
					precedent.addClass('active');
					precedent.show();
				}
			}
		};
		$('#stop').click(function(){
			carroussel.stop();
		})
		$('#play').click(function(){
			carroussel.play();
		})
		$('#next').click(function(){
			carroussel.next();
		})
		$('#prev').click(function(){
			carroussel.prev();
		})
	//***********************************
	$('body .panel').collapse();
	
	//**********mes plugins*************************
	
			//**********affiche des codes apres achats*************
				jQuery.fn.affiche_code = function(){
					var th = $(this);
					var liste = '';
					var users = localStorage.getItem('num_users');
					$.ajax({
							url:'http://etudiant8fala.esy.es/documents/model/recup_identifiants.php',
							type:'post',
							dataType:'json',
							data:'users='+users,
							success:function(data){
								var montableau = {};
								for(var t in data){
									montableau[data[t].pharmacies_num] = [];
									for(var u in data){
										if(data[t].pharmacies_num == data[u].pharmacies_num){
											montableau[data[t].pharmacies_num].push(data[u].identifiant);
										}
									}
								}
								for(var i in montableau){
									var identifiants = '';
									for(var p =0;p<montableau[i].length;p++){
										if(p == montableau[i].length-1){
											identifiants += montableau[i][p];
										}else{
											identifiants += montableau[i][p]+' | ';
										}
									}
									liste += '<tr>\
												<td>'+i+'</td>\
												<td>'+identifiants+'</td>\
											 </tr>';
								}
								$('body #corps_identifiants').html(liste);
							}
						})
				}
			//************************
			
			//******centre la photo de profil*******************	
				jQuery.fn.met_tof_milieu = function (){
					var photo = localStorage.getItem('photo');
					$('#photo_profil').html('<img src="http://etudiant8fala.esy.es/documents/maison/'+photo+'" class="img-circle img-responsive" title="" alt="profil" style="width:position:absolute;100%;height:100%">');
					$("#photo_profil").css({
						'position':'absolute',
						'display':'inline-block',
						'line-height':'40px',
						'height':'40px',
						'width':'50px',
						'left':'50%',
						'top':'0px',
						'transform':'translatex(-50%)'
					})
				}
			//****************************************
			
			//********affiche l accueil(liste de nos pharmacies)**********
				jQuery.fn.tagant_affiche_accueil = function(){
							var hauteur = screen.height/2;
							$('#accueil').attr('name','actif');
							$('#affichepagecommande,#afficahge,#affichepagenavigationrecherche,#affichepagenavigationcode').attr('style','display:none');
							$('#affichepagenavigationaccueil').removeAttr('style');
							$('body #affichepagenavigationaccueil').css({
								'height':'200px',
								'text-align':'center',
								'font-size':'1.2em'
							});
							$('body #affichepagenavigationaccueil').html('<div><img src="imagespub/ajax-loader.gif" style="position:absolute;top:50%;left:50%;transform:translate(-50%,-50%)"><br><span>Veuillez patienter...<span/><div>');
							function erreur_request(){
								var hauteur = screen.height/2;
								$('#accueil').attr('name','actif');
								$('#afficahge,#affichepagenavigationrecherche,#affichepagenavigationcode').attr('style','display:none');
								$('#affichepagenavigationaccueil').removeAttr('style');
								$('body #affichepagenavigationaccueil').css({
									'text-align':'center',
									'height':hauteur+'px'
								});
								$('body #affichepagenavigationaccueil').html('<img src="imagespub/ajax-loader.gif" style="position:absolute;top:50%;left:50%;transform:translate(-50%,-50%)"><br><span class="alert alert-warning">Veuillez verifier votre connexion internet.</span>');
							}
							function success_request(position){
								var contenir = '';
								handleResults = function(results,status){
										var ville = false;
										var pays = false;
									main : for(var i in results[0]){
										if(i ==='address_components'){
											for(var j=0,m=results[0][i].length;j<m;j++){
												var qui_est_tu = results[0][i][j].long_name;
												for(var a=0,b=results[0][i][j].types.length;a<b;a++){
													if(results[0][i][j].types[a] ==='country'){
														pays = qui_est_tu;
													}else if(results[0][i][j].types[a] =='locality'){
														ville = qui_est_tu;
													}
												}
											}
											break main;
										}
									}
									if(ville && pays){
										sessionStorage.setItem('ville',ville);
										sessionStorage.setItem('pays',pays);
										$.ajax({
											url:'http://etudiant8fala.esy.es/documents/model/selectAll.php',
											type:'post',
											dataType:'json',
											data:'pays='+pays+'&ville='+ville,
											success:function(data){
												for(var i =0;i< data.length;i++){
													var longueur = (data[i].nom).length-1;
													if(pays == 'Cameroun'){
														data[i].nom = data[i].nom.substr(0,longueur-3);
													}else if(pays == 'Gabon'){
														data[i].nom = data[i].nom.substr(0,longueur-2);
													}
													contenir += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="width:100%;background-color:white">\
																<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="position:relative;min-height:18.8vh;"><img src="http://etudiant8fala.esy.es/documents/maison/'+data[i].photo1+'"	class="img-circle" style="position:absolute;display:block;top:50%;left:50%;transform:translate(-50%,-50%);margin:auto;height:80%;width:80%"></div>\
																<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9" style="border-bottom:0.1px solid gray">\
																	<div class="row">\
																		<div style="min-height:9vh;"><span style="font-size:1.1em;color:black;margin-top:4%" class="pull-left">'+data[i].nom+'</span></div>\
																	</div>\
																	<div class="row">\
																		<div id="'+data[i].nom+'" style="min-height:9vh;color:" class="col-xs-5 col-sm-5 col-md-5 col-lg-5 Rspecifique"><button class="btn" style="background-color:#c8d6a4">Rechercher</button></div>\
																		<div style="min-height:9vh;" name="'+data[i].nom+'" class="col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xs-5 col-sm-5 col-md-5 col-lg-5 affiche_detail" id="'+data[i].num_pharmacies+'"><button class="btn" style="background-color:#c8d6a4">Detail</button></div>\
																	</div>\
																</div>\
															</div>';
												}
												$('#accueil').css({
													 'border-bottom':'4px solid yellow'
												 })
												$('body #affichepagenavigationaccueil').html(contenir);
												$('body #accueil').trigger('click');
											}
										})
									}
								}
								var geocoder = new google.maps.Geocoder();
								geocoder.geocode({
									location : new google.maps.LatLng(position.coords.latitude,position.coords.longitude)
								},handleResults);
							}
							 if(navigator.geolocation){
								navigator.geolocation.getCurrentPosition(success_request,erreur_request,{maximumAge:30000,enableHighAccuracy:true});
							}
							
						}
			//*******************************************************
			
			//affiche form_inscription,a_propos,form_connexion******************
				jQuery.fn.tagant_affiche_infos = function(bouton,chemin_page){
					this.on('click',bouton,function(e){
						e.preventDefault();
						var th= $(this);
						var url = chemin_page;
						$.ajax({
							url:url,
							type:'post',
							data:'',
							success:function(data){
								if(bouton == '#recherche'){
									$("#panel_right").removeAttr('style');
									$("#panel_right").removeClass('masque_right');
									$("section").attr('style','background:gray;opacity:0.5');
									$('body #corprechercheSP').html(data);
									$('body').tagant_affiche_villes();
								}else{
									$('#infos .modal-body').html(data);
								}
							}
						})
					})
				}
			//****************************************	
			
			//***********traite form_inscription*********	
				jQuery.fn.tagant_submit_form_photos2 = function(form_soumis2){
					this.on('submit',form_soumis2,function(e){
						e.preventDefault();
						
						var th= $(this);
						var url = th.data('url');
						var ff = $('form')[0];
						var formas = new FormData(ff);
						var numusers = localStorage.getItem('num_users');
						var nom_pharmacie = sessionStorage.getItem('nompharmacie');
						var pays = sessionStorage.getItem('pays');
						formas.append("num_users",numusers);
						formas.append("nom_pharmacie",nom_pharmacie);
						formas.append("pays",pays);
						$.ajax({
									url:url,
									data:formas,
									processData:false,
									contentType:false,
									type:'post',
									dataType:'json',
									success:function(data){
										if(confirm('Ordonance envoyée avec success')){
											$('body #accueil').trigger('click');
										}
										// if(typeof(data[0].prenom) != 'undefined'){
											// for(var i in data[0]){
												// localStorage.setItem(i,data[0][i]);
											// }
											// $('#ferme').trigger('click');
											// $('body').met_tof_milieu();
											// $('#alerteur').html('<center>Bienvenu <strong> '+data[0].prenom+'!</strong> !</center>');
											// $("#alerteur").fadeIn(1000).delay(3000).fadeOut(1000);
										// }else{
											// alert("Verifier vos donneés entreés");
										// }
										
									}
								})
							})
						}
						$('body').tagant_submit_form_photos2('#envoiphotoordonnance');
				//************************************************
				
				//************traite soumission form connexion*****
					jQuery.fn.tagant_submit_form = function(form_soumis){//for connexion
						this.on('submit',form_soumis,function(e){
							e.preventDefault();
							var th= $(this);
							var url = th.data('url');
							$.ajax({
								url:url,
								type:'post',
								dataType:'json',
								data:th.serialize(),
								success:function(data){
									if(data.nom != '' && typeof(data.nom) != 'undefined'){
										for(var i in data){
												localStorage.setItem(i,data[i]);
											}
											$('#ferme').trigger('click');
											$('#accueil').trigger('click');
											$('body').met_tof_milieu();
											$('#alerteur').html('<center>Bienvenu <strong> '+data.prenom+'!</strong> !</center>');
											$("#alerteur").fadeIn(1000).delay(3000).fadeOut(1000);							
									}else{
											alert("Verifier vos donneés entreés");
										}
									}
								})
							})
						}
			//**************fin traitement connexion*********
			
			//******affiche les villes******************
						jQuery.fn.tagant_affiche_villes = function(){
							$.ajax({
											url:'http://etudiant8fala.esy.es/documents/model/affiche_les_villes.php',
											type:'post',
											dataType:'json',
											data:'pays='+sessionStorage.getItem('pays'),
											success:function(data){
												var liste="<option>Choix ville</option>";
												var u = '';
												var w = 0;
												var ville = sessionStorage.getItem('ville');
												for(var i =0;i< data.length;i++){
													if(data[i].nom_ville == ville){
														w = data[i].id_ville;
													}else{
														u += '<option value="'+data[i].id_ville+'"  class="liste_villes">'+data[i].nom_ville+'</option>';
													}
												}
												liste += '<option value="'+w+'"  class="liste_villes">'+ville+'</option>';
												liste += u;
												$("body #first_form .ville").html(liste);
												$("body #form_rechercheSP .ville").html(liste);
											}
										})
							
							
					}
					$('body').tagant_affiche_villes();
			//*************************************
			
			//affiche les details de la pharmacie choisi
				jQuery.fn.tagant_affiche_detail_pharmacies = function(){
					this.on('click','.affiche_detail',function(e){
						e.preventDefault();
						var th = $(this);
						var id = th.attr('id');
						var detail = '';
						var comment = '';
						var caroussel_photos = '';
						var verifie = 0;
						var nom = th.attr('name');
						$.ajax({
							url:'http://etudiant8fala.esy.es/documents/controleur/traite_detail_pharmacies.php',
							type:'post',
							dataType:'json',
							data:'id='+id,
							success:function(data){
								var tab = [];
									for(var j in data[0]){
										if(j == 'photo1' || j == 'photo2' || j == 'photo3' || j == 'photo4'){
											tab.push(data[0][j]);
										}
									}
									for(var h =0;h< tab.length;h++){
										if(h == 0){
											caroussel_photos += '<div class="slide active" name="'+h+'">\
																	 <div class="image" style="position:relative;min-height:25vh;">\
																		<img style="position:absolute;display:block;top:50%;left:50%;transform:translate(-50%,-50%);margin:auto;height:100%;width:100%" alt = "" src="http://etudiant8fala.esy.es/documents/maison/'+tab[h]+'" class="img-responsive">\
																	 </div>\
																</div>';
										}else{
											caroussel_photos += '<div class="slide col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display:none;position:relative;min-height:25vh;" name="'+h+'">\
																	 <div class="image">\
																		<img style="position:absolute;display:block;top:0px;left:0px;margin:auto;height:100%;width:100%" alt = "" src="http://etudiant8fala.esy.es/documents/maison/'+tab[h]+'" class="img-responsive">\
																	 </div>\
																</div>';
										}
									}
									for(var i in data[0]){
											if(i == 'heure_ouverture' && data[0][i] != '00:00:00'){
												var ide = 'Heure d\'ouverture';
												detail += '<div class="form-group">\
														<label>'+ide+':</label>  <span>'+data[0][i]+'</span>\
												   </div>';
											}else if(i == 'heure_fermeture' && data[0][i] != '00:00:00'){
												var ide = 'Heure de fermeture';
												detail += '<div class="form-group">\
														<label>'+ide+':</label>  <span>'+data[0][i]+'</span>\
												   </div>';
											}else if(i == 'jours_ouvrable'){
												var ide = 'Jours ouvrables';
												detail += '<div class="form-group">\
														<label>'+ide+':</label>  <span>'+data[0][i]+'</span>\
												   </div>';
											}else if(i == 'contact'){
												detail += '<div class="form-group">\
														<label>'+i+':</label>  <span>'+data[0][i]+'</span>\
												   </div>';
											}else if(i == 'horaire'){
												detail += '<div class="form-group">\
														<label>'+i+':</label>  <span>'+data[0][i]+'</span>\
												   </div>';
											}else if(i == 'email' && data[0][i] != ''){
												detail += '<div class="form-group">\
														<label>'+i+':</label>  <span>'+data[0][i]+'</span>\
												   </div>';
											}else if(i == 'position'){
												detail += '<div class="form-group">\
														<label>'+i+':</label>  <span>'+data[0][i]+'</span>\
												   </div>';
												}
										}
											carroussel.stop();
											
											$("button[name='affichetoidsdetail']").removeAttr('id');
											$("button[name='affichetoidsdetail']").attr('id',nom);
											
											$('#affichepagenavigationaccueil,#affichepagenavigationcode,#affichepagenavigationrecherche').attr('style','display:none');
											$("#affichagedetailpharmacies").removeAttr('style');
											$("#carroussel_detail").removeAttr('style');
											$("#afficheRspecifique").attr('style','display:none');
											$("#detail").html(detail);
											$('#comment').html(comment);
											$('#carroussel').html(caroussel_photos);
											$('#outils center').html('<span style="font-size:1.2em">'+nom+'</span>');
											carroussel.play();
									}
								})
							})
						}
			//fin affichage detail
			//***********pour la recherche specifique**********
			jQuery.fn.tagant_recherche_specifique = function(){
					 this.on('submit','#form_rechercheSP',function(e){
						 e.preventDefault();
						 var th= $(this);
						 var url = th.data('url');
						 var contenir= '';
							$.ajax({
										url:url,
										type:'post',
										dataType:'json',
										data:th.serialize(),
										success:function(data){
											for(var i =0;i< data.length;i++){
												contenir += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="width:100%;background-color:#466652;border-bottom:2px solid black">\
															<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="position:relative;min-height:18.8vh;"><img src="http://etudiant8fala.esy.es/documents/maison/'+data[i].photo1+'"	class="img-rounded" style="position:absolute;display:block;top:50%;left:50%;transform:translate(-50%,-50%);margin:auto;height:80%;width:80%"></div>\
															<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">\
																<div class="row">\
																	<div style="min-height:9vh;"><span style="font-size:1.1em;color:white" class="pull-left">'+data[i].nom+'</span></div>\
																</div>\
																<div class="row">\
																	<div id="'+data[i].nom+'" style="min-height:9vh;color:" name ="recherchepoussee" class="col-xs-5 col-sm-5 col-md-5 col-lg-5 Rspecifique"><button class="btn" style="background-color:#c8d6a4">Rechercher</button></div>\
																	<div style="min-height:9vh;" name="'+data[i].nom+'" class="col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xs-5 col-sm-5 col-md-5 col-lg-5 affiche_detail" id="'+data[i].num_pharmacies+'"><button class="btn" style="background-color:#c8d6a4">Detail</button></div>\
																</div>\
															</div>\
														</div>';
											}
											$('#afficheRspecifique,#affichagedetailpharmacies,#affichepagenavigationaccueil,#affichepagenavigationcode,#affichepagenavigationrecherche').attr('style','display:none');
											$("#affichepoussee").removeAttr('style');
											$("#retouralaccueil").attr('name','recherchepoussee');
											$("#affichepoussee").html(contenir);
											$('#ferme_recherche').trigger('click');
										}
									})
					 })
				}
			//*********************************
	//***********************fin des plugins*************************
	
})(jQuery)