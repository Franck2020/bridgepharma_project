$(function(){
		//*******************************affiche les heures de commande*****************************************
		 function ouvre(){
			 var db;
			  document.addEventListener('deviceready',function myDeviceReady(){
				 db = window.openDatabase("commandes","1.0","mes commandes",3*1024*1024);
				 db.transaction(selectDB2,null,null);
			}, false);
		 }
		 function ouvre1(){
			 var db;
			document.addEventListener('deviceready',function myDeviceReady(){
				 db = window.openDatabase("commandes","1.0","mes commandes",3*1024*1024);
				 db.transaction(selectDB,null,null);
			}, false);
		 }
		  function selectDB2(tx){
			 var vdate = sessionStorage.getItem('vdate');
			 tx.executeSql('SELECT * FROM COMMANDES WHERE datess = ? ORDER BY NUM_CMD DESC',[vdate],renderList2,null);
		 }
		  function renderList2(tx,results){
			  $('#letitre center').text(sessionStorage.getItem('titredetailcommande'));
			  var len = results.rows.length;
			  var montableau = [];
			  var htmlstring='';
			  for(var i=0;i<len;i++){
				  var madate = ''+new Date(results.rows.item(i).dateauthentique)+'';
				  if(montableau.length != 0){
					  var dismoi =0;
					  var compte =0;
					   
					  for(var i1 in montableau){
						  compte++;
						  if(madate == montableau[i1]){
							  dismoi++;
						  }
						  if(compte == montableau.length && dismoi == 0){
							  montableau.push(madate);
						  }
					  }
				  }else{
					  montableau.push(madate);
				  }
			  }
			  
			 for(var p =0;p<montableau.length;p++){
				 var datec = new Date(montableau[p]);
				var heure = datec.getHours();
				var minute = datec.getMinutes();
				var seconde = datec.getSeconds();
				var datecc = heure+':'+minute+':'+seconde;
				 htmlstring += "<li class='list-group-item heure_de_commande' style='background-Color:#54c767;cursor:pointer'><span>"+datecc+"</span><span style='display:none'>"+montableau[p]+"</span></li>";
			 }
			$('#listecmd').html(htmlstring);
		  }
		//*********************************************************************************** 
		 
		//**************************** affiche les jours de commande****************************** 
		 function selectDB(tx){
			 tx.executeSql('SELECT * FROM COMMANDES ORDER BY NUM_CMD DESC',[],renderList,null);
		 }
		 function renderList(tx,results){
			 var htmlstring ='';
			 var len = results.rows.length;
				var date = new Date();
				var annee = date.getFullYear();
				var mois = date.getMonth();
				mois = mois +1;
				var jour = date.getDate();
				var ladate = jour+'/'+mois+'/'+annee;
				var tabdate = [];
			
			for(var i=0;i<len;i++){
				var nbreegalite = 0;
				 var date2 = new Date(results.rows.item(i).dateauthentique);
				 var annee2 = date2.getFullYear();
				 var mois2 = date2.getMonth();
				 mois2 = mois2 +1;
				 var jour2 = date2.getDate();
				 var heure2 = date2.getHours();
				 var minute2 = date2.getMinutes();
				 var ladate2 = jour2+'/'+mois2+'/'+annee2;
				 for(var u in tabdate){
					 if(ladate2 == tabdate[u]){
						 nbreegalite++;
					 }
				 }
				 if(nbreegalite == 0){
					 tabdate.push(ladate2);
					 if(ladate == ladate2){
						 var date22 = ""+date2+"";
						htmlstring += "<li class='list-group-item jour_de_commande' style='background-Color:#54c767;cursor:pointer' name="+ladate2+">Aujourd'hui</li>";
					 }else if(annee == annee2 && mois == mois2 && jour-jour2 == 1){
						 htmlstring += "<li class='list-group-item jour_de_commande' style='background-Color:#54c767;cursor:pointer' name="+ladate2+">Hier</li>";
					 }
					 else{
						  htmlstring += "<li class='list-group-item jour_de_commande' style='background-Color:#54c767;cursor:pointer' name="+ladate2+">"+ladate2+'</li>';
					 }
				 }
			 }
				$('#messagesarrives,#affichepagenavigationaccueil,#affichagedetailpharmacies,#affichepoussee,#affichepagenavigationcode,#affichepagenavigationrecherche').attr('style','display:none');
				$('header').attr('style','display:none');
				$('footer').attr('style','display:none');
				$('#navigation').attr('style','display:none');
				$('#affichepagecommande').removeAttr('style');
				$('#listecmd').html(htmlstring);
		 }
		//*********************************************************************************************************** 
		
		//*****************************affiche les commandes *****************************************
				 function ouvre2(){
					 var db;
					  document.addEventListener('deviceready',function myDeviceReady(){
						 db = window.openDatabase("commandes","1.0","mes commandes",3*1024*1024);
						 db.transaction(selectDB3,null,null);
					}, false);
				 }
				  function selectDB3(tx){
					 var vdate = new Date(sessionStorage.getItem('vdate2'));
					 tx.executeSql('SELECT * FROM COMMANDES WHERE dateauthentique = ? ORDER BY NUM_CMD DESC',[vdate],renderList3,null);
				 }
				  function renderList3(tx,results){
					 var textes =  $('#letitre center').text();
					  $('#letitre center').html(textes+'<span class="pull-right"> A-> '+sessionStorage.getItem('titredetailcommande2')+'</span>');
					  var len = results.rows.length;
					  var htmlstring='';
					  for(var i=0;i<len;i++){
						  var valpharm='';
						  var nomphar = '';
						  var prixtotal = '';
						  var identifiantpharm = '';
						  
						  nomphar = results.rows.item(i).nom_pharmacie;
						  prixtotal = results.rows.item(i).prixtotal;
						  identifiantpharm = results.rows.item(i).identifiantcmd;
						  
						  valpharm += "<br><br><br><li class='list-group-item'>Identifiant  -> "+identifiantpharm+"</li>";
						  valpharm += "<li class='list-group-item'>Prix total  -> "+prixtotal+"  FCFA</li>";
						  
						  htmlstring += '<div class="panel panel-info">\
												<div class="panel-heading col-xs-12 col-sm-12 col-md-12 col-lg-12">\
													<h1 class="panel-title"><center>'+nomphar+'</center></h1>\
												</div>\
												<div class="panel-body">'+valpharm+'</div>\
										</div>';
					  }
					  
					$('#listecmd').html(htmlstring);
		  }
		
		//********************************************************************************************
		 
		$('body').on('click','.jour_de_commande',function(){
			var nom = $(this).attr('name');
			var texte = $(this).text();
			sessionStorage.setItem('titredetailcommande',texte);
			sessionStorage.setItem('vdate',nom);
			ouvre();
			$('body .faitretour').attr('id','jourcommande');
		})
		$('body').on('click','.heure_de_commande',function(){
			var t = $(this);
			var texte = t.children(':eq(0)');
			texte = texte.text();
			var nom = t.children(':eq(1)');
			nom = nom.text();
			sessionStorage.setItem('titredetailcommande2',texte);
			sessionStorage.setItem('vdate2',nom);
			ouvre2();
			$('body .faitretour').attr('id','heurecmd');
		})
		
		$('body').on('click','#commandes',function(){
			ouvre1();
			$('body .faitretour').attr('id','accueil');
		})
		
		$('#retourversdatecommandes').click(function(){
			$('#datecommandes').removeAttr('style');
			$('#tabcommandes').attr('style','display:none');
		})
		
		$('body').on('click','.faitretour',function(){
			var id = $(this).attr('id');
			if(id == 'accueil'){
				// $('#affichepagecommande,#messagesarrives,#affichagedetailpharmacies,#affichepoussee,#affichepagenavigationcode,#affichepagenavigationrecherche').attr('style','display:none');
				// $('#affichepagenavigationaccueil').removeAttr('style');
				// $('header').removeAttr('style');
				// $('footer').removeAttr('style');
				// $('#navigation').removeAttr('style');
				// $('body').tagant_affiche_accueil();
				location.reload(true);
			}else if(id == 'jourcommande'){
				ouvre1();
				$('body .faitretour').attr('id','accueil');
			}else if(id == 'heurecmd'){
				ouvre();
				$('body .faitretour').attr('id','jourcommande');
			}
		})
		 
})