$(function(){
	document.addEventListener('deviceready',myDeviceReady,false);
	function myDeviceReady(){
		locationpos();
	}
	
	function contientout(){
		if(localStorage.getItem('nom') !=null){
			$.ajax({
				url:'vue/vue_application.html',
				type:'post',
				data:'',
				success:function(data){
					$('#mesvues').html(data);
					$('body').tagant_affiche_accueil();
					 $('body').tagant_recherche_specifique();
					 localStorage.setItem('rechercheaffichee','non');
					 localStorage.setItem('codeaffichee','non');
					 $('.carousel').carousel();
				}
			})
		}else{
				$('#enregistrement').trigger('click');
					$('body').on('submit','#form_enregistrement',function(e){
						e.preventDefault();
						var th= $(this);
						var url = th.data('url');
						var ff = $('form')[0];
						var formas = new FormData(ff);
						$.ajax({
									url:url,
									data:formas,
									processData:false,
									contentType:false,
									type:'post',
									dataType:'json',
									success:function(data){
										if(typeof(data[0].prenom) != 'undefined'){
											for(var i in data[0]){
												localStorage.setItem(i,data[0][i]);
											}
										}
										$.ajax({
											url:'vue/vue_application.html',
											type:'post',
											data:'',
											success:function(data){
												$('#mesvues').html(data);
												$('body').tagant_affiche_accueil();
												$('body').tagant_recherche_specifique();
												localStorage.setItem('rechercheaffichee','non');
												localStorage.setItem('codeaffichee','non');
												$('.carousel').carousel();
											}
										})
									}
								})
							})
						}
	}
	
	function onRequestSuccess(success){
		contientout();
	}

	function onRequestFailure(error){
		console.error("Accuracy request failed: error code="+error.code+"; error message="+error.message);
		if(error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED){
			if(window.confirm("Failed to automatically set Location Mode to 'High Accuracy'. Would you like to switch to the Location Settings page and do this manually?")){
				cordova.plugins.diagnostic.switchToLocationSettings();
			}
		}
	}
	
	
	function locationpos(){
		cordova.plugins.locationAccuracy.request(onRequestSuccess, onRequestFailure, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);
	}
 })

